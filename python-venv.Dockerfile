FROM python-pkg-mgmt_python-base:latest

RUN apt-get install -y virtualenvwrapper

RUN echo "source /usr/share/virtualenvwrapper/virtualenvwrapper.sh" >> ~/.bashrc

COPY /* ./

# bash's rc files are odd -- bashrc is only read in interactive non-login shells, and there isn't a nice equivalent to
# .zshenv, so we force bash to be in interactive mode with -i
RUN bash -ic 'mkvirtualenv -p /usr/bin/python3.7 demo-proj && workon demo-proj && pip3 install -r requirements.txt'

CMD ["bash", "-ic", "workon demo-proj && jupyter notebook --ip=0.0.0.0 --port=9001 --allow-root"]

