FROM ubuntu:disco

RUN apt-get update

RUN apt install -y locales python3-pip

# python likes having locale set
RUN locale-gen en_US.UTF-8
ENV LANG=en_US.utf8 LC_ALL=en_US.UTF-8

RUN mkdir /demo-proj
WORKDIR /demo-proj
