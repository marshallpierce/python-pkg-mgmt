FROM python-pkg-mgmt_python-base:latest

RUN apt-get install -y pipenv

COPY /* ./

RUN pipenv install

CMD ["pipenv", "run", "jupyter", "notebook", "--ip=0.0.0.0", "--port=9002", "--allow-root"]
