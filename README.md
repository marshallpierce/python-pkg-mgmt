A demo repo for python package management.

# Python and virtualenvs

See `docker-compose.yml` and the various `*.Dockerfile` files to compare 3 different ways of installing Python packages:
- via `pip install`
- using a virtualenv with [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/)
- using [Pipenv](https://pipenv.readthedocs.io/en/latest/)

Run `./gen-file-lists.sh` and compare the `file-list-*.txt` output to see the difference between the base OS and each of the different ways of installing packages.

- See what's in the base OS (ubuntu): 
    - `less file-list-base.txt`
- Compare installing a package globally vs the base os: 
    - `diff file-list-base.txt file-list-global-pip.txt | less`
- Compare installing a package globally vs installing with Pipenv:
    - `diff file-list-global-pip.txt file-list-pipenv.txt | less`
    - Also try `vimdiff` instead of `diff` if you're a vim user for a side by side diff. Type `<esc>`  `:qa` `<enter>` to exit.
- etc...

## Demo project layout

- `src` directory: python source files and notebooks
    - Mark this as a "Source" folder in your IDE
- `Pipfile` and `requirements.txt`: two ways of achieving similar goals.
    - If you're using Pipenv (recommended), you'll be using the Pipfile to specify which packages .
    - If you're using virtualenvwrapper, you'll use `requirements.txt`
