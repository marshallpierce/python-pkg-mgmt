#!/bin/sh

set -ex

docker-compose build

for container in $(echo 'base global-pip venv-pip pipenv' | tr ' ' '\n')
do
docker-compose run python-${container} find / -type f | grep -vE '/(proc|sys)' | sort > file-list-${container}.txt
done
