FROM python-pkg-mgmt_python-base:latest

COPY /* ./

RUN pip3 install -r requirements.txt

CMD ["jupyter", "notebook", "--ip=0.0.0.0", "--port=9000", "--allow-root"]
